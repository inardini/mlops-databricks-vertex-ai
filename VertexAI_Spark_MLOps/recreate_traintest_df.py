# Databricks notebook source
# MAGIC %run ../VertexAI_Spark_MLOps/1_utils

# COMMAND ----------

# cast Size from int to double type, create train/test df 
final_dataset_df = sqlContext.table(final_dataset_schemaname+"."+final_dataset_tablename)
final_dataset_df = final_dataset_df.withColumn("Size_double", final_dataset_df['Size'].cast("double"))

# COMMAND ----------

# split train and test data
(train, test) = final_dataset_df.randomSplit([0.8, 0.2])
print("We have "+str(train.count())+" training examples and "+str(test.count())+" test examples.")

# COMMAND ----------


