# Databricks notebook source
# MAGIC %run ../VertexAI_Spark_MLOps/1_utils

# COMMAND ----------

query = 'DROP SCHEMA IF EXISTS {} CASCADE'.format(final_dataset_schemaname)
sqlContext.sql(query)
